<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */
use phpOMS\Validation\Validator;

// @todo: why am i doing this here and not in the controller????
/**
 * @var \Web\Views\Page\GenericView $this
 */
$navObj = \Modules\Navigation\Models\Navigation::getInstance($this->request, $this->app->dbPool);

$nav = new \Modules\Navigation\Views\NavigationView($this->app, $this->request, $this->response);
$nav->setTemplate('/Modules/Navigation/Theme/Backend/top');
$nav->setNav($navObj->getNav());
$nav->setLanguage($this->l11n->getLanguage());
$top = $nav->render();

$nav->setTemplate('/Modules/Navigation/Theme/Backend/side');
$side = $nav->render();
$head = $this->response->getHead();

$dispatch = $this->getData('dispatch');
?>
<!DOCTYPE HTML>
<html>
<head>
    <?= $head->getMeta()->render(); ?>
    <title><?= $a = $head->getTitle(); ?></title>
    <?= $head->renderAssets(); ?>
    <style>
        <?= $head->renderStyle(); ?>
    </style>
    <script><?= $head->renderScript(); ?></script>
</head>
<body>
<div class="vh" id="dim"></div>
<div id="h">
    <div id="bar-s">
        <?= $top; ?>
    </div>
    <div id="bar-b">
        <span class="vC" id="ham-trigger">
            <label for="nav-trigger"><i class="fa fa-bars"></i></label>
        </span>
        <span class="vC" id="logo" itemscope itemtype="http://schema.org/Organization"><a
                href="<?= \phpOMS\Uri\UriFactory::build('/{/lang}/backend'); ?>"
                itemprop="legalName"><?= $this->getData('Name') ?></a>
        </span>
        <span class="vC" id="s-bar" role="search">
            <label> <input type="text" autofocus="autofocus"> </label>
            <input type="submit" value="<?= $this->l11n->lang[0]['Search'] ?>">
        </span>
        <span class="vC" id="u-box">
            <img class="rf" src="<?= '/Web/Backend/img/default-user.jpg'; ?>">
        </span>

        <div id="u-logo" itemscope itemtype="http://schema.org/Person"></div>
    </div>
</div>
<div id="out">
    <?= $side; ?>
    <input type="checkbox" id="nav-trigger" class="nav-trigger"<?= !$this->request->isMobile() ? ' checked' : ''; ?>>
    <main>
        <?php if (isset($dispatch[\phpOMS\Views\ViewLayout::MAIN])) {
            foreach ($dispatch[\phpOMS\Views\ViewLayout::MAIN] as $value) {
                echo $value->render();
            }
        } else {
            // TODO: return error page? 404!?
        } ?>
    </main>
</div>
<?= $head->renderAssetsLate(); ?>

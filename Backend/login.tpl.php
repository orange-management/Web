<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */
$head = $this->response->getHead();
?>
<!DOCTYPE HTML>
<html>
<head>
    <?= $head->getMeta()->render(); ?>
    <title><?= $a = $head->getTitle(); ?></title>
    <?= $head->renderAssets(); ?>
    <style>
        <?= $head->renderStyle(); ?>
    </style>
    <script><?= $head->renderScript(); ?></script>
    <style type="text/css">
        html, body {
            height: 100%;
            font-family: arial, serif;
            background: #2F2F2F;
            color: #fff;
        }

        form label {
            color: #fff;
            text-shadow: none;
        }

        .floater {
            float: left;
            height: 50%;
            width: 100%;
            margin-bottom: -130px;
        }

        #parent {
            display: table;
            position: static;
            clear: left;
            height: 230px;
            width: 450px;
            margin: 0 auto;
        }

        #child {
            display: table-cell;
            vertical-align: middle;
            width: 100%;
            position: relative;
            font-size: 1.0em;
        }

        #title {
            text-align: center;
            font-size: 10em;
            padding-bottom: 20px;
        }

        h1 {
            margin-bottom: 30px;
        }
    </style>
</head>
<body>
<div class="floater"></div>
<div id="parent">
    <div id="child">
        <h1>Orange Management</h1>
        <div class="floatLeft">
            <img src="/Web/Backend/img/logo.png" width="200px">
        </div>
        <div class="floatLeft" style="padding: 30px 0 0 50px">
            <form method="<?= \phpOMS\Message\RequestMethod::POST; ?>" action="<?= \phpOMS\Uri\UriFactory::build('/{/lang}/api/login?csrf={$CSRF}'); ?>">
                <table class="layout">
                    <tr><td><label for="iName"><?= $this->l11n->lang[0]['Username']; ?></label>
                    <tr><td><input id="iName" type="text" name="user" value="admin">
                    <tr><td><label for="iPassword"><?= $this->l11n->lang[0]['Password']; ?></label>
                    <tr><td><input id="iPassword" type="password" name="pass" value="orange">
                    <tr><td><input type="submit" value="<?= $this->l11n->lang[0]['Login']; ?>">
                </table>
            </form>
        </div>
    </div>
</div>
</body>
</html>

<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */
namespace Web\Backend;

use phpOMS\Asset\AssetType;
use phpOMS\DataStorage\Database\DatabaseStatus;
use phpOMS\Localization\Localization;
use phpOMS\Message\Http\Request;
use phpOMS\Message\Http\Response;
use phpOMS\Message\RequestMethod;
use phpOMS\System\File\PathException;
use phpOMS\Validation\Validator;
use Web\Views\Page\GenericView;
use Web\WebApplication;

/**
 * Application class.
 *
 * @category   Framework
 * @package    Framework
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @license    OMS License 1.0
 * @link       http://orange-management.com
 * @since      1.0.0
 */
class Application
{
    /**
     * WebApplication.
     *
     * @var WebApplication
     * @since 1.0.0
     */
    private $app = null;

    /**
     * Config.
     *
     * @var array
     * @since 1.0.0
     */
    private $config = null;

    /**
     * Constructor.
     *
     * @param WebApplication $app    WebApplication
     * @param array          $config Application config
     *
     * @since  1.0.0
     * @author Dennis Eichhorn <d.eichhorn@oms.com>
     */
    public function __construct(WebApplication $app, array $config)
    {
        $this->app          = $app;
        $this->config       = $config;
        $this->app->appName = 'Backend';
    }

    /**
     * Rendering backend.
     *
     * @param Request  $request  Request
     * @param Response $response Response
     *
     * @return void
     *
     * @since  1.0.0
     * @author Dennis Eichhorn <d.eichhorn@oms.com>
     */
    public function run(Request $request, Response $response)
    {
        $pageView = new GenericView($this->app, $request, $response);

        /* Backand only allows GET */
        if ($request->getMethod() !== RequestMethod::GET) {
            $response->setStatusCode(406);

            $pageView->setTemplate('/Web/Backend/Error/406');
            $response->set('Content', $pageView);

            return;
        }

        /* Database OK? */
        if ($this->app->dbPool->get()->getStatus() !== DatabaseStatus::OK) {
            $response->setStatusCode(503);

            $pageView->setTemplate('/Web/Backend/Error/503');
            $response->set('Content', $pageView);

            return;
        }

        /* CSRF token OK? */
        if ($request->getData('CSRF') !== null && $this->app->sessionManager->get('CSRF') !== $request->getData('csrf')) {
            $response->setStatusCode(403);

            return;
        }

        $options = $this->app->appSettings->get([1000000009, 1000000029]);
        $account = $this->app->accountManager->get($request->getAccount());

        $l11n = new Localization();
        $l11n->setLanguage(!in_array($request->getL11n()->getLanguage(), $this->config['language']) ? $options[1000000029] : $request->getL11n()->getLanguage());
        $account->setL11n($l11n);
        $response->setL11n($l11n);

        /* Load theme language */
        if (($path = realpath($oldPath = __DIR__ . '/lang/' . $response->getL11n()->getLanguage() . '.lang.php')) === false) {
            throw new PathException($oldPath);
        }

        /** @noinspection PhpIncludeInspection */
        include $path;
        /** @var array $THEMELANG Theme language */
        $this->app->l11nManager->loadLanguage($response->getL11n()->getLanguage(), 0, $THEMELANG);

        /* Load core language */
        if (($path = realpath($oldPath = __DIR__ . '/../../phpOMS/Localization/lang/' . $response->getL11n()->getLanguage() . '.lang.php')) === false) {
            throw new PathException($oldPath);
        }

        /** @noinspection PhpIncludeInspection */
        include $path;
        /** @var array $CORELANG Framework language elements */
        $this->app->l11nManager->loadLanguage($response->getL11n()->getLanguage(), 0, $CORELANG);

        /* TODO: WHY AM I DOING THIS? Carefull we are setting an array here that's why changes in the future will result in different values */
        $response->getL11n()->setLang($this->app->l11nManager->getLanguage($l11n->getLanguage()));

        $head    = $response->getHead();
        $baseUri = $request->getUri()->getBase();

        /* Load assets */
        $head->addAsset(AssetType::CSS, $baseUri . 'Resources/fontawesome/css/font-awesome.min.css');
        $head->addAsset(AssetType::CSS, $baseUri . 'cssOMS/styles.css');
        /* @todo: this should be loaded in one file for releases. right now this is done for easier debugging purposes */
        // Framework
        $head->addAsset(AssetType::JS, $baseUri . 'jsOMS/Autoloader.js');
        $head->addAsset(AssetType::JS, $baseUri . 'jsOMS/Account/AccountType.enum.js');
        $head->addAsset(AssetType::JS, $baseUri . 'jsOMS/Account/AccountManager.js');
        $head->addAsset(AssetType::JS, $baseUri . 'jsOMS/Asset/AssetManager.js');
        $head->addAsset(AssetType::JS, $baseUri . 'jsOMS/Uri/UriFactory.js');
        $head->addAsset(AssetType::JS, $baseUri . 'jsOMS/DataStorage/CacheManager.js');
        $head->addAsset(AssetType::JS, $baseUri . 'jsOMS/DataStorage/CookieJar.js');
        $head->addAsset(AssetType::JS, $baseUri . 'jsOMS/DataStorage/LocalStorage.js');
        $head->addAsset(AssetType::JS, $baseUri . 'jsOMS/DataStorage/StorageManager.js');
        $head->addAsset(AssetType::JS, $baseUri . 'jsOMS/Event/EventManager.js');
        $head->addAsset(AssetType::JS, $baseUri . 'jsOMS/Message/Request/RequestData.enum.js');
        $head->addAsset(AssetType::JS, $baseUri . 'jsOMS/Message/Request/BrowserType.enum.js');
        $head->addAsset(AssetType::JS, $baseUri . 'jsOMS/Message/Request/OSType.enum.js');
        $head->addAsset(AssetType::JS, $baseUri . 'jsOMS/Message/Request/RequestMethod.enum.js');
        $head->addAsset(AssetType::JS, $baseUri . 'jsOMS/Message/Request/RequestType.enum.js');
        $head->addAsset(AssetType::JS, $baseUri . 'jsOMS/Message/Request/Request.js');
        $head->addAsset(AssetType::JS, $baseUri . 'jsOMS/Message/Request/RequestManager.js');
        $head->addAsset(AssetType::JS, $baseUri . 'jsOMS/Message/Response/ResponseType.enum.js');
        $head->addAsset(AssetType::JS, $baseUri . 'jsOMS/Message/Response/ResponseResultType.enum.js');
        $head->addAsset(AssetType::JS, $baseUri . 'jsOMS/Message/Response/ResponseManager.js');
        $head->addAsset(AssetType::JS, $baseUri . 'jsOMS/Message/Response/Response.js');
        $head->addAsset(AssetType::JS, $baseUri . 'jsOMS/Module/ModuleFactory.js');
        $head->addAsset(AssetType::JS, $baseUri . 'jsOMS/Module/ModuleManager.js');
        $head->addAsset(AssetType::JS, $baseUri . 'jsOMS/Dispatcher/Dispatcher.js');
        $head->addAsset(AssetType::JS, $baseUri . 'jsOMS/Log/Logger.js');
        $head->addAsset(AssetType::JS, $baseUri . 'jsOMS/Log/LogLevel.enum.js');
        $head->addAsset(AssetType::JS, $baseUri . 'jsOMS/UI/FormManager.js');
        $head->addAsset(AssetType::JS, $baseUri . 'jsOMS/UI/Input/Keyboard/KeyboardManager.js');
        $head->addAsset(AssetType::JS, $baseUri . 'jsOMS/UI/Input/InputManager.js');
        $head->addAsset(AssetType::JS, $baseUri . 'jsOMS/UI/Input.js');
        $head->addAsset(AssetType::JS, $baseUri . 'jsOMS/UI/TableManager.js');
        $head->addAsset(AssetType::JS, $baseUri . 'jsOMS/UI/TabManager.js');
        $head->addAsset(AssetType::JS, $baseUri . 'jsOMS/UI/Loader.js');
        $head->addAsset(AssetType::JS, $baseUri . 'jsOMS/UI/Input/InputManager.js');
        $head->addAsset(AssetType::JS, $baseUri . 'jsOMS/UI/UIManager.js');
        $head->addAsset(AssetType::JS, $baseUri . 'jsOMS/Utils/oLib.js');
        $head->addAsset(AssetType::JS, $baseUri . 'jsOMS/Views/FormView.js');

        // Models
        $head->addAsset(AssetType::JS, $baseUri . 'Model/Message/Dom.js');
        $head->addAsset(AssetType::JS, $baseUri . 'Model/Message/FormValidation.js');
        $head->addAsset(AssetType::JS, $baseUri . 'Model/Message/NotifyType.js');
        $head->addAsset(AssetType::JS, $baseUri . 'Model/Message/Notify.js');
        $head->addAsset(AssetType::JS, $baseUri . 'Model/Message/Redirect.js');
        $head->addAsset(AssetType::JS, $baseUri . 'Model/Message/Reload.js');

        $head->addAsset(AssetType::JS, $baseUri . 'Web/Backend/js/backend.js');
        $head->setScript('core', $script = 'var Url = "' . $baseUri . '", assetManager = new jsOMS.Asset.AssetManager();');
        $response->setHeader('content-security-policy', 'script-src \'self\' \'sha256-' . base64_encode(hash('sha256', $script, true)) . '\' frame-src \'self\'', true);

        $head->addAsset(AssetType::CSS, $baseUri . 'Web/Backend/css/backend.css');
        $head->addAsset(AssetType::JS, $baseUri . 'Resources/d3/d3.min.js');
        $head->setStyle('core', file_get_contents(__DIR__ . '/css/backend-small.css'));

        /* Handle not logged in */
        if ($account->getId() < 1) {
            $pageView->setTemplate('/Web/Backend/login');
            $response->set('Content', $pageView);

            return;
        }

        /* Get routed modules, initialize them and load their language */
        $modules = $this->app->moduleManager->getRoutedModules($request);
        $this->app->moduleManager->initModule($modules);
        $this->app->moduleManager->loadLanguage($response->getL11n()->getLanguage(), $this->app->appName);

        /* Set main view */
        $pageView->setData('Name', $options[1000000009]);
        $pageView->setData('Title', 'Orange Management');
        $pageView->setData('Destination', $request->getPath(1));

        $pageView->setTemplate('/Web/Backend/index');
        $response->set('Content', $pageView);

        /* Loading all navigation files */
        $languages = $this->app->moduleManager->getLanguageFiles($request);

        foreach ($languages as $path) {
            if ($path[strlen($path) - 1] === '/') {
                // Is not a navigation file
                continue;
            }

            $path = realpath($oldPath = __DIR__ . '/../..' . $path . '.' . $response->getL11n()->getLanguage() . '.lang.php');

            if ($path === false || Validator::startsWith($path, ROOT_PATH) === false || strpos($path, 'config.php') !== false) {
                throw new PathException($oldPath);
            }

            /** @noinspection PhpIncludeInspection */
            include $path;
            /** @var string[][] $MODLANG Language array */
            $this->app->l11nManager->loadLanguage($response->getL11n()->getLanguage(), 'Navigation', $MODLANG);
        }

        /* Dispatch all views (not finalized yet) */
        $dispatched = $this->app->dispatcher->dispatch($this->app->router->route($request->getRoutify(), $request->getMethod()), $request, $response);

        $pageView->addData('dispatch', $dispatched);
    }
}

(function (jsOMS, undefined) {
    jsOMS.Application = function () {
        jsOMS.Autoloader.initPreloaded();

        this.cacheManager    = new jsOMS.DataStorage.CacheManager();
        this.cookieJar       = new jsOMS.DataStorage.CookieJar();
        this.storageManager  = new jsOMS.DataStorage.StorageManager();
        this.eventManager    = new jsOMS.Event.EventManager();
        this.responseManager = new jsOMS.Message.Response.ResponseManager();
        this.requestManager  = new jsOMS.Message.Request.RequestManager();
        this.dispatcher      = new jsOMS.Dispatcher.Dispatcher();
        this.assetManager    = new jsOMS.Asset.AssetManager();
        this.acountManager   = new jsOMS.Account.AccountManager();
        this.uiManager       = new jsOMS.UI.UIManager(this);
        this.inputManager    = new jsOMS.UI.Input.InputManager();
        this.logger          = new jsOMS.Log.Logger();
        this.moduleManager   = new jsOMS.Module.ModuleManager(this);

        this.responseManager.add('notify', notifyMessage);
        this.responseManager.add('validation', formValidationMessage);
        this.responseManager.add('redirect', redirectMessage);
        this.responseManager.add('reload', reloadMessage);

        this.uiManager.bind();
    };
}(window.jsOMS = window.jsOMS || {}));

jsOMS.ready(function() {
    window.omsApp = new jsOMS.Application();
});

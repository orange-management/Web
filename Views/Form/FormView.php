<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */
namespace Web\Views\Form;

use phpOMS\ApplicationAbstract;
use phpOMS\Message\RequestAbstract;
use phpOMS\Message\RequestMethod;
use phpOMS\Message\ResponseAbstract;
use Web\Views\WebViewAbstract;

/**
 * Form view.
 *
 * @category   Theme
 * @package    Framework
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @license    OMS License 1.0
 * @link       http://orange-management.com
 * @since      1.0.0
 */
class FormView extends WebViewAbstract
{

    /**
     * Elements.
     *
     * @var array
     * @since 1.0.0
     */
    protected $elements = [];

    /**
     * Has own submit button.
     *
     * @var bool
     * @since 1.0.0
     */
    protected $hasSubmit = true;

    /**
     * Has own submit button.
     *
     * @var bool
     * @since 1.0.0
     */
    protected $onChange = false;

    /**
     * Url for request.
     *
     * @var string
     * @since 1.0.0
     */
    protected $action = '';

    /**
     * Id.
     *
     * @var string
     * @since 1.0.0
     */
    protected $formId = '';

    /**
     * Class.
     *
     * @var string
     * @since 1.0.0
     */
    protected $class = '';

    /**
     * Submit buttons.
     *
     * @var array
     * @since 1.0.0
     */
    protected $submit = [];

    /**
     * Request method.
     *
     * @var RequestMethod
     * @since 1.0.0
     */
    protected $method = RequestMethod::POST;

    /**
     * Formfields.
     *
     * @var array
     * @since 1.0.0
     */
    protected $formFields = null;

    /**
     * {@inheritdoc}
     */
    public function __construct(ApplicationAbstract $app, RequestAbstract $request, ResponseAbstract $response)
    {
        parent::__construct($app, $request, $response);
    }

    /**
     * @param mixed $row    Element row
     * @param mixed $column Element column
     *
     * @return array
     *
     * @since  1.0.0
     * @author Dennis Eichhorn <d.eichhorn@oms.com>
     */
    public function getElement($row, $column) : array
    {
        return $this->elements[$row][$column];
    }

    /**
     * @param mixed $row     Element row
     * @param mixed $column  Element column
     * @param array $element Element
     *
     * @return void
     *
     * @since  1.0.0
     * @author Dennis Eichhorn <d.eichhorn@oms.com>
     */
    public function setElement($row, $column, array $element)
    {
        $this->elements[$row][$column] = $element;
    }

    /**
     * @param array $fields Form fields
     *
     * @return void
     *
     * @since  1.0.0
     * @author Dennis Eichhorn <d.eichhorn@oms.com>
     */
    public function setFormFields(array $fields)
    {
        $this->formFields = $fields;
    }

    /**
     * @return array
     *
     * @since  1.0.0
     * @author Dennis Eichhorn <d.eichhorn@oms.com>
     */
    public function getElements() : array
    {
        return $this->elements;
    }

    /**
     * @param mixed $elements
     *
     * @return void
     *
     * @since  1.0.0
     * @author Dennis Eichhorn <d.eichhorn@oms.com>
     */
    public function setElements($elements)
    {
        $this->elements = $elements;
    }

    /**
     * @return bool
     *
     * @since  1.0.0
     * @author Dennis Eichhorn <d.eichhorn@oms.com>
     */
    public function isHasSubmit() : bool
    {
        return $this->hasSubmit;
    }

    /**
     * @param bool $hasSubmit
     *
     * @return void
     *
     * @since  1.0.0
     * @author Dennis Eichhorn <d.eichhorn@oms.com>
     */
    public function setHasSubmit(bool $hasSubmit)
    {
        $this->hasSubmit = $hasSubmit;
    }

    /**
     * @return string
     *
     * @since  1.0.0
     * @author Dennis Eichhorn <d.eichhorn@oms.com>
     */
    public function getAction() : string
    {
        return $this->action;
    }

    /**
     * @param string $action
     *
     * @return void
     *
     * @since  1.0.0
     * @author Dennis Eichhorn <d.eichhorn@oms.com>
     */
    public function setAction(string $action)
    {
        $this->action = $action;
    }

    /**
     * @param string $formId
     *
     * @return void
     *
     * @since  1.0.0
     * @author Dennis Eichhorn <d.eichhorn@oms.com>
     */
    public function setFormId(string $formId)
    {
        $this->formId = $formId;
    }

    /**
     * @param string $class
     *
     * @return void
     *
     * @since  1.0.0
     * @author Dennis Eichhorn <d.eichhorn@oms.com>
     */
    public function setClass(string $class)
    {
        $this->class = $class;
    }

    /**
     * @return RequestMethod
     *
     * @since  1.0.0
     * @author Dennis Eichhorn <d.eichhorn@oms.com>
     */
    public function getMethod() : RequestMethod
    {
        return $this->method;
    }

    /**
     * @param RequestMethod|string $method
     *
     * @return void
     *
     * @since  1.0.0
     * @author Dennis Eichhorn <d.eichhorn@oms.com>
     */
    public function setMethod(string $method)
    {
        $this->method = $method;
    }

    /**
     * @return bool
     *
     * @since  1.0.0
     * @author Dennis Eichhorn <d.eichhorn@oms.com>
     */
    public function isOnChange() : bool
    {
        return $this->onChange;
    }

    /**
     * @param bool $onChange
     *
     * @return void
     *
     * @since  1.0.0
     * @author Dennis Eichhorn <d.eichhorn@oms.com>
     */
    public function setOnChange(bool $onChange)
    {
        $this->onChange = $onChange;
    }

    /**
     * @param string $name
     * @param string $content
     * @param array  $options
     *
     * @return void
     *
     * @since  1.0.0
     * @author Dennis Eichhorn <d.eichhorn@oms.com>
     */
    public function setSubmit(string $name, string $content, array $options = null)
    {
        $this->submit[$name] = [$content, $options];
    }
}

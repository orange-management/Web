<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */
namespace Web\Views\Divider;

use phpOMS\ApplicationAbstract;
use phpOMS\Message\RequestAbstract;
use phpOMS\Message\ResponseAbstract;
use Web\Views\WebViewAbstract;

/**
 * Form view.
 *
 * @category   Theme
 * @package    Framework
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @license    OMS License 1.0
 * @link       http://orange-management.com
 * @since      1.0.0
 */
class TabularView extends WebViewAbstract
{

    /**
     * Active tab.
     *
     * @var string
     * @since 1.0.0
     */
    protected $active = 1;

    /**
     * Tab views.
     *
     * @var string[]
     * @since 1.0.0
     */
    protected $tab = [];

    /**
     * {@inheritdoc}
     */
    public function __construct(ApplicationAbstract $app = null, RequestAbstract $request, ResponseAbstract $response)
    {
        parent::__construct($app, $request, $response);
    }

    /**
     * Creating tab.
     *
     * @param string $title Tab title
     * @param string $view  View to display
     * @param string $id    Tab id
     *
     * @return array
     *
     * @since  1.0.0
     * @author Dennis Eichhorn <d.eichhorn@oms.com>
     */
    public function addTab(string $title, string $view, string $id = null)
    {
        if (!isset($id)) {
            $id = $title;
        }

        $this->tab[$id]['title']   = $title;
        $this->tab[$id]['content'] = $view;
    }
}

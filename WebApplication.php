<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */
namespace Web;

use Model\Account;
use Model\CoreSettings;
use phpOMS\Account\AccountManager;
use phpOMS\ApplicationAbstract;
use phpOMS\DataStorage\Cache\Pool as CachePool;
use phpOMS\DataStorage\Database\Pool;
use phpOMS\DataStorage\Session\HttpSession;
use phpOMS\Dispatcher\Dispatcher;
use phpOMS\Event\EventManager;
use phpOMS\Localization\L11nManager;
use phpOMS\Localization\Localization;
use phpOMS\Log\FileLogger;
use phpOMS\Message\Http\Request;
use phpOMS\Message\Http\Response;
use phpOMS\Message\RequestDestination;
use phpOMS\Module\ModuleManager;
use phpOMS\Router\Router;
use phpOMS\System\MimeType;
use phpOMS\Uri\Http;

/**
 * Application class.
 *
 * @category   Framework
 * @package    Framework
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @license    OMS License 1.0
 * @link       http://orange-management.com
 * @since      1.0.0
 */
class WebApplication extends ApplicationAbstract
{

    /**
     * Constructor.
     *
     * @param array $config Core config
     *
     * @since  1.0.0
     * @author Dennis Eichhorn <d.eichhorn@oms.com>
     */
    public function __construct(array $config)
    {
        set_exception_handler(['\Web\UnhandledHandler', 'exceptionHandler']);
        set_error_handler(['\Web\UnhandledHandler', 'errorHandler']);
        register_shutdown_function(['\Web\UnhandledHandler', 'shutdownHandler']);
        mb_internal_encoding('UTF-8');

        $sub = new \Web\E500\Application($this, $config);

        $response = new Response();
        $response->setHeader('content-security-policy', 'script-src \'self\' frame-src \'self\'');
        $response->setHeader('x-xss-protection', '1; mode=block');
        $response->setHeader('x-content-type-options', 'nosniff');
        $response->setHeader('x-frame-options', 'SAMEORIGIN');
        // todo: implement public key pins

        $uri      = new Http(Http::getCurrent());
        $uri->setRootPath($config['page']['root']);

        $request = new Request($uri);

        // Setting up default language for any unexpected behaviour
        $l11n = new Localization();
        $l11n->setLanguage(!in_array($request->getL11n()->getLanguage(), $config['language']) ? 'en' : $request->getL11n()->getLanguage());
        $response->setL11n($l11n);

        try {
            $this->dbPool = new Pool();

            $request->init();
            $this->dbPool->create('core', $config['db']['core']['masters'][0]);

            $this->cachePool      = new CachePool($this->dbPool);
            $this->appSettings    = new CoreSettings($this->dbPool->get());
            $this->eventManager   = new EventManager();
            $this->router         = new Router();
            $this->sessionManager = new HttpSession(36000);
            $this->moduleManager  = new ModuleManager($this);
            $this->l11nManager    = new L11nManager();
            $this->accountManager = new AccountManager();
            $this->dispatcher     = new Dispatcher($this);
            $this->logger         = FileLogger::getInstance(ROOT_PATH . '/Logs');
            $account              = new Account(0, $this->dbPool->get(), $this->sessionManager, $this->cachePool);
            $account->authenticate();
            $aid = $this->accountManager->set($account);
            $request->setAccount($aid);
            $response->setAccount($aid);

            switch ($request->getPath(1)) {
                case strtolower(RequestDestination::API):
                    $sub = new \Web\Api\Application($this, $config);
                    break;
                case strtolower(RequestDestination::BACKEND):
                    $sub = new \Web\Backend\Application($this, $config);
                    break;
                case strtolower(RequestDestination::WEBSITE):
                    $sub = new \Web\Website\Application($this, $config);
                    break;
                case strtolower(RequestDestination::REPORTER):
                    $sub = new \Web\Reporter\Application($this, $config);
                    break;
                default:
                    $sub = new \Web\E404\Application($this, $config);
                    break;
            }
        } catch (\Exception $e) {
            $sub = new \Web\E500\Application($this, $config);
        } finally {
            $sub->run($request, $response);
            $response->pushHeader();

            if (strpos($response->getHeader('Content-Type')[0], MimeType::M_JSON) !== false) {
                echo $response->toJson();
            } elseif (strpos($response->getHeader('Content-Type')[0], MimeType::M_CSV) !== false) {
                echo $response->toCsv();
            } else {
                echo $response->render();//preg_replace('/\s+/', ' ', $response->render());
            }
        }
    }
}

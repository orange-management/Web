<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */
/**
 * @var \Web\Views\Form\FormView $this
 */
?>
<form action="<?= $this->action; ?>" 
    method="<?= $this->method; ?>" 
    id="<?= $this->formId; ?>" 
    class="<?= $this->class; ?>"
    <?= isset($this->formFields) ? ' data-formfields=\'' . json_encode($this->formFields) . '\'' : ''; ?>>
    <?php include 'FormInner.tpl.php'; ?>
</form>

<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */ /** @var \Web\Views\Lists\ListView $this */ ?>
<table class="tc-1 m-<?= $this->module; ?> mp-<?= ($this->module + $this->pageId); ?>"
       id="i-<?= ($this->module + $this->id); ?>">
    <?php foreach ($this->elements as $rKey => $row): ?>
        <tr>
            <th>
                <label><?= $row[0]; ?></label>
            </th>
            <?php for ($i = 1; $i < count($row); $i++): ?>
                <td><?= $row[$i]; ?></td>
            <?php endfor; ?>
        </tr>
    <?php endforeach; ?>
</table>

<?php
/**
 * Orange Management
 *
 * PHP Version 7.0
 *
 * @category   TBD
 * @package    TBD
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @copyright  2013 Dennis Eichhorn
 * @license    OMS License 1.0
 * @version    1.0.0
 * @link       http://orange-management.com
 */
namespace Web\Api;

use Model\Message\Redirect;
use Model\Message\Reload;
use phpOMS\Auth\LoginReturnType;
use phpOMS\DataStorage\Database\DatabaseStatus;
use phpOMS\Localization\Localization;
use phpOMS\Message\Http\Request;
use phpOMS\Message\Http\Response;
use phpOMS\System\MimeType;
use Web\Views\Page\GenericView;
use Web\WebApplication;

/**
 * Application class.
 *
 * @category   Framework
 * @package    Framework
 * @author     OMS Development Team <dev@oms.com>
 * @author     Dennis Eichhorn <d.eichhorn@oms.com>
 * @license    OMS License 1.0
 * @link       http://orange-management.com
 * @since      1.0.0
 */
class Application
{
    /**
     * WebApplication.
     *
     * @var WebApplication
     * @since 1.0.0
     */
    private $app = null;

    /**
     * Config.
     *
     * @var array
     * @since 1.0.0
     */
    private $config = null;

    /**
     * Constructor.
     *
     * @param WebApplication $app    WebApplication
     * @param array          $config Application config
     *
     * @since  1.0.0
     * @author Dennis Eichhorn <d.eichhorn@oms.com>
     */
    public function __construct(WebApplication $app, array $config)
    {
        $this->app          = $app;
        $this->config       = $config;
        $this->app->appName = 'Api';
    }

    /**
     * Rendering backend.
     *
     * @param Request  $request  Request
     * @param Response $response Response
     *
     * @return void
     *
     * @since  1.0.0
     * @author Dennis Eichhorn <d.eichhorn@oms.com>
     */
    public function run(Request $request, Response $response)
    {
        $response->setHeader('Content-Type', 'text/plain; charset=utf-8');
        $pageView = new GenericView($this->app, $request, $response);

        if ($this->app->dbPool->get()->getStatus() !== DatabaseStatus::OK) {
            $response->setStatusCode(503);

            return;
        }

        /* Checking csrf token, if a csrf token is required at all has to be decided in the controller */
        if ($request->getData('CSRF') !== null && $this->app->sessionManager->get('CSRF') !== $request->getData('CSRF')) {
            $response->setStatusCode(403);

            return;
        }

        $options = $this->app->appSettings->get([1000000009, 1000000029]);
        $account = $this->app->accountManager->get($request->getAccount());

        $l11n = new Localization();
        $l11n->setLanguage(!in_array($request->getL11n()->getLanguage(), $this->config['language']) ? $options[1000000029] : $request->getL11n()->getLanguage());
        $account->setL11n($l11n);
        $response->setL11n($l11n);

        if (($uris = $request->getUri()->getQuery('r')) !== null) {
            $request_r = clone $request;
            $uris      = json_decode($uris, true);

            foreach ($uris as $key => $uri) {
                $request_r->init($uri);

                $modules = $this->app->moduleManager->getRoutedModules($request_r);
                $this->app->moduleManager->initModule($modules);
                $this->app->moduleManager->loadLanguage($response->getL11n()->getLanguage(), $this->app->appName);

                $this->app->dispatcher->dispatch($this->app->router->route($request->getRoutify(), $request->getMethod()), $request, $response, null);
            }
        } else {
            if ($request->getPath(2) === 'login' && $account->getId() < 1) {
                $response->setHeader('Content-Type', MimeType::M_JSON . '; charset=utf-8', true);

                $login = $account->login($request->getData('user') ?? '', $request->getData('pass') ?? '');
                if ($login === LoginReturnType::OK) {
                    $response->set($request->__toString(), new Reload());
                } else {
                    // TODO: create login failure msg
                    echo $login;
                }

                return;
            } elseif ($request->getPath(2) === 'logout' && $request->getData('csrf') === $this->app->sessionManager->get('CSRF')) {
                $response->setHeader('Content-Type', MimeType::M_JSON . '; charset=utf-8', true);

                $this->app->sessionManager->remove('UID');
                $this->app->sessionManager->save();

                $response->set($request->__toString(), new Redirect('http://www.google.de'));

                return;
            }

            // TODO: handle not logged in
            // not logged in should still forward certain requests just as guest user
            // let modules handle the rest???!?!?!?!
            if ($account->getId() < 1) {
                return;
            }

            $modules = $this->app->moduleManager->getRoutedModules($request);
            $this->app->moduleManager->initModule($modules);
            $this->app->moduleManager->loadLanguage($response->getL11n()->getLanguage(), $this->app->appName);

            $dispatched = $this->app->dispatcher->dispatch($this->app->router->route($request->getRoutify(), $request->getMethod()), $request, $response);

            $pageView->addData('dispatch', $dispatched);
        }
    }
}
